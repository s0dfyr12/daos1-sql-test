package sqltest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestSQL {
	public static void main(String[] args) throws SQLException {

		SQLcon sql = new SQLcon();

		sql.open(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = sql.query("SELECT * FROM Medlem");
		while (rs.next()) {
			System.out.println("Navn: " + rs.getString("navn"));
		}
		sql.close();

	}
}