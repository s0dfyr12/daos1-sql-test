package sqltest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Lek05Opg1 {
	public static void main(String[] args) throws SQLException {

		SQLcon sql = new SQLcon();

		sql.open(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = sql.query("SELECT navn, adresse, telefon FROM Instruktor");
		while (rs.next()) {
			System.out.println("Navn: " + rs.getString("navn") + " Adresse:  " + rs.getString("adresse") + " Telefon:  "
					+ rs.getString("telefon"));
		}
		sql.close();

	}
}